import React, { Component } from 'react';
import Todoinput from './Todoinput';
import Todolist from './Todolist';
import * as uuid from 'uuid';
import Header from './Header';

class Todos extends Component {
  constructor() {
    super();
    this.state = {
      items: [],
      filterItems: [],
      item: '',
      searchItem:'',
      id: 0,
      createdAt: '',
      sortType: {
        item: 'asc',
      },
    };
  }

  handleChange = (e) => {
    if(e.target.value.length <= 30) {
this.setState({item:e.target.value});
    }
  }

  searchHandleChange = (e) => {
    this.setState({searchItem: e.target.value},
      ()=>this.filterData());
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.item === '') {
      alert('please enter some text');
    }
    const newItem = {
      id: this.state.id,
      item: this.state.item,
      createdAt: new Date().toLocaleString(),
    };
    const updatedItems = [...this.state.items, newItem];
    this.setState({
      items: updatedItems,
      filterItems : updatedItems,
      item: '',
      id: uuid.v4(),
      createdAt: '',
    });
  };

  filterData = () => {
    var searchItem = this.state.searchItem;
    var datas = this.state.items;
    if(searchItem.length > 0){
      datas = datas.filter(entry => Object.values(entry).some(val => typeof val === "string" && val.includes(searchItem)));
  }
      this.setState({filterItems : datas})
  }
 
  sortBy = (key) => {
    this.setState({
      items: this.state.items.sort((a, b) => {
        let comparison = a[key].toUpperCase() > b[key].toUpperCase() ? 1 : -1;
        return this.state.sortType[key] === 'asc' ? comparison : -comparison;
      }),
      sortType: {
        [key]: this.state.sortType[key] === 'asc' ? 'desc' : 'asc',
      },
    });
  };

  render() {
    return (
      <div>
        <Header />
        <form className="form-inline ml-auto d-flex justify-content-center">
          <input
            className="form-control mr-sm-2 mt-2 mb-2"
            name="searchWord"
            value={this.state.searchItem}
            type="text"
            placeholder="search todos here"
            onChange={this.searchHandleChange}
          />
        </form>
        <Todoinput
          item={this.state.item}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
        <Todolist filterItems={this.state.filterItems} sortBy={this.sortBy} />
      </div>
    );
  }
}

export default Todos;
