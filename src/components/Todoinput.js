import React from 'react';

function Todoinput(props) {
  const { item, handleChange, handleSubmit } = props;
  return (
    <div className="container bg-dark mt-3 mb-3">
      <form className="d-flex p-2" onSubmit={handleSubmit}>
        <div className="container d-flex justify-content-between">
          <input
            type="text"
            placeholder="Enter here"
            value={item}
            onChange={handleChange}
          />
          <button className="btn btn-primary" type="submit">
            Add Todo Item
          </button>
        </div>
      </form>
    </div>
  );
}
export default Todoinput;
