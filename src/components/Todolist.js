import React from 'react';

function Todolist(props) {
  const { filterItems, sortBy } = props;
  return (
    <div>
      <table className="table table-hover">
        <thead>
          <tr>
            <th>
              <button
                type="button"
                className="btn btn-secondary"
                onClick={() => sortBy('item')}
              >
                TODO
              </button>
            </th>
            <th>
              <button
                type="button"
                className="btn btn-secondary"
                onClick={() => sortBy('createdAt')}
              >
                Created At
              </button>
            </th>
          </tr>
        </thead>
        {filterItems.map((data) => (
          <tbody key={data.id}>
            <tr>
              <td>{data.item}</td>
              <td>{data.createdAt}</td>
            </tr>
          </tbody>
        ))}
      </table>
    </div>
  );
}
export default Todolist;
