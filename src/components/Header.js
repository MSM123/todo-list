import React from 'react';

function Header() {
  return (
    <div>
      <header style={headerStyle}>
        <h1>TodoList</h1>
      </header>
    </div>
  );
}

const headerStyle = {
  textAlign: 'center',
  backgroundColor: '#0000A0',
  padding: '10px',
  color: '#ff0000',
};

export default Header;
